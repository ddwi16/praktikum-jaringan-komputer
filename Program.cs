﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class SimpleUdpClient
{
    public static void Main()
    {
        // Deklarasi Variabel Username dan Pesan
        byte[] data = new byte[1024];
        string input, stringData;
        int recv;

        // Bind Server
        IPEndPoint ipep = new IPEndPoint(
                        IPAddress.Parse("10.252.39.148"), 8080);
        Socket server = new Socket(AddressFamily.InterNetwork,
                       SocketType.Dgram, ProtocolType.Udp);

        // Input Username untuk Client
        Console.Write("Input Your Name : ");
        string username = Console.ReadLine();

        // Mengirim Username ke Server
        data = Encoding.ASCII.GetBytes(username);
        server.SendTo(data, data.Length, SocketFlags.None, ipep);

        // Menerima pesan bahwa server telah terhubung ke socket
        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
        EndPoint Remote = (EndPoint)sender;
        recv = server.ReceiveFrom(data, ref Remote);

        // Menerima Username Server dan Menampilkannya
        Console.WriteLine("Message received from {0}:", Remote.ToString());
        string ServerName = Encoding.ASCII.GetString(data, 0, data.Length);
        Console.WriteLine("Hi, I'm " + Encoding.ASCII.GetString(data, 0, data.Length));

        while (true)
        {
            // Input Pesan untuk Dikirim ke Server
            Console.Write(username + " : ");
            input = Console.ReadLine();
            if (input == "exit")
                break;
            // Mengirim Pesan ke Server
            server.SendTo(Encoding.ASCII.GetBytes(input), Remote);
            data = new byte[1024];
            // Menerima Pesan dari Server
            recv = server.ReceiveFrom(data, ref Remote);
            stringData = Encoding.ASCII.GetString(data, 0, recv);
            // Menampilkan Pesan dan Username Server
            Console.WriteLine(ServerName + " : " + stringData);
        }
        Console.WriteLine("Stopping client");
        server.Close();
    }
}
